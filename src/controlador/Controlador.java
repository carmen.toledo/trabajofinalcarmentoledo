/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import javax.swing.JTextArea;

/**
 *
 * @author carmen
 */
public class Controlador {

    //Obtener el número de procesadores
    public int numProcesador() {
        //Es equivalente a este comando: lscpu -p | egrep -v '^#' | sort -u -t, -k 2,4 | wc -l
        //Para determinar el numero de procesadores:
        int procesadores = Runtime.getRuntime().availableProcessors() / 2;
        return procesadores;
    }

    //Compilar el código
    public void compilar(String path, JTextArea jTextArea) {
        Process p;
        try {
            String comando = "mpicc " + path;
            p = Runtime.getRuntime().exec(comando);
            jTextArea.setText("> Archivo compilado.");
        } catch (Exception e) {
        }
    }

    //Ejecutar el código
    public void ejecutar(int numProcesador, JTextArea jTextArea) {
        Process p;
        String mostrar = "";
        try {
            String comando = "mpirun -np " + numProcesador + "" + " a.out";
            p = Runtime.getRuntime().exec(comando);
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String ejecucion = "";
            while ((ejecucion = br.readLine()) != null) {
                mostrar += ejecucion + "\n";
            }
            jTextArea.setText(mostrar);
            p.waitFor();
        } catch (Exception e) {
        }
    }

}
